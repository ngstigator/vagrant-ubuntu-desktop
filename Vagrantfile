# encoding: utf-8
# -*- mode: ruby -*-
# vi: set ft=ruby :

ANSIBLE_PATH = __dir__ # absolute path to Ansible directory on host machine

require 'yaml'

# Read varaibles from file
vconfig = YAML.load_file("#{ANSIBLE_PATH}/vagrant.default.yml")

Vagrant.configure("2") do |config|
  config.vm.box = vconfig.fetch('vagrant_box')
  config.vm.network "private_network", ip: vconfig.fetch('vagrant_ip')
  config.vm.hostname = vconfig.fetch('vagrant_hostname')

  config.vm.provider :virtualbox do |vb|
    vb.gui = true
    vb.memory = vconfig.fetch('vagrant_memory')
    vb.cpus = vconfig.fetch('vagrant_cpus')
    vb.customize ["modifyvm", :id, "--vram", "48"]
    vb.customize ["modifyvm", :id, "--clipboard-mode", "bidirectional"]
  end

  # Install minimal requirements
  # config.vm.provision "shell", inline: "sudo apt-get install -y python"

  # Optional NFS. Make sure to remove other synced_folder line too
  if Vagrant::Util::Platform.windows?
    config.vm.synced_folder "C:\\Users\\" + vconfig.fetch('vagrant_hostuser') + "\\Shared", "/media/Shared", :nfs => { :mount_options => ["dmode=777","fmode=666"] }
  elsif Vagrant::Util::Platform.darwin?
    config.vm.synced_folder "/Users/" + vconfig.fetch('vagrant_hostuser') + "/Shared", "/media/Shared", :nfs => { :mount_options => ["dmode=777","fmode=666"] }
  elsif Vagrant::Util::Platform.linux?
    config.vm.synced_folder "/home/" + vconfig.fetch('vagrant_hostuser') + "/Shared", "/media/Shared", :nfs => { :mount_options => ["dmode=777","fmode=666"] }
  end

  # SSH
  # Copy local public key(s) to guest
  config.vm.provision "file", source: "./import_keys.pub", destination: "/home/vagrant/.ssh/import_keys.pub"

  # append import key(s) to authorized_keys and remove duplicates
  config.vm.provision :shell, privileged: false, path: "scripts/sshkeys.sh"
  config.ssh.forward_agent = true

end
