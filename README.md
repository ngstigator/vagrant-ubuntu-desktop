## Description

Build an Ubuntu VM desktop using Vagrant and Ansible.

---

## Prerequisites

1. [Git](https://git-scm.com/)
2. [Vagrant](https://www.vagrantup.com/)
3. [VirtualBox](https://www.virtualbox.org/)
4. [Ansible](https://www.ansible.com/resources/get-started)

---

## Usage

1. Clone repository.
2. Add your local public key(s) into file **import_keys.pub**. This is required to run Ansible playbooks.
3. Run `vagrant up`
4. Install ansible requirements `ansible-galaxy install -r requirements.yml`
5. Run playbook `ansible-playbook setup.yml -i hosts/ubuntu -u vagrant`
