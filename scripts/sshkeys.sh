#!/bin/bash
# append import key(s) to authorized_keys and remove duplicates
cd /home/vagrant/.ssh
cat import_keys.pub >> authorized_keys
sort authorized_keys | uniq > public_keys.pub
mv public_keys.pub authorized_keys
chmod 644 authorized_keys
rm *.pub